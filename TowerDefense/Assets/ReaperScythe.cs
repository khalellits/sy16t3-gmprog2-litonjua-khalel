﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ReaperScythe : MonoBehaviour {

	public float MinDuration;
	public float MaxDuration;
	public Image CoolDown;

	public GameObject[] enemies;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		CoolDown.fillAmount = MinDuration / MaxDuration;

		enemies = GameObject.FindGameObjectsWithTag("Enemy");

		MinDuration += Time.deltaTime * 10f;

		if (MinDuration >= 100f)
		{
			MinDuration = 100;
		}

		ReaperScytheActivate();

	}

	public void ReaperScytheActivate() //PASSIVE
    {
		 foreach (GameObject enemy in enemies)
		 {
			if (MinDuration >= 100f)
			{	
				

				int rand = Random.Range(0, enemies.Length);
					Debug.Log(rand);
				if (enemies[rand].GetComponent<EnemyHealth>().currHp <= enemies[rand].GetComponent<EnemyHealth>().maxHp * .15f)
				{
					Debug.Log(rand);
					enemies[rand].GetComponent<EnemyHealth>().currHp = 0; 
					enemies[rand].GetComponent<Enemy>().Gold();
					
				}
				MinDuration = 0F;
			}
        }
    }
}
