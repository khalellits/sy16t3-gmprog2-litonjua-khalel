﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnMeteor : MonoBehaviour {

	public GameObject meteorite;
	public bool active;

	public Image CoolDown;

	public float MinDuration;
	public float MaxDuration;


	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		CoolDown.fillAmount = MinDuration/MaxDuration;	

		MinDuration += Time.deltaTime * 20f;

		if (MinDuration >= 100f)
		{
			MinDuration = 100f;
		}
	}

	void OnMouseUp()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if (MinDuration >= 100F)
		{
			if (active)
			{	
				MinDuration = 0;
				active = false;
				if (Physics.Raycast(ray, out hit, 100))
				{
					Debug.DrawLine(ray.origin, hit.point);
					Instantiate(meteorite, new Vector3(hit.point.x,hit.point.y + 20f,hit.point.z),transform.rotation);
					meteorite.transform.position = Vector3.Lerp(ray.origin, hit.point, Time.deltaTime);
					return;
				}
			}
		
		}
	}

	public void activate(bool enable)
	{
		active = enable;
	}

}
