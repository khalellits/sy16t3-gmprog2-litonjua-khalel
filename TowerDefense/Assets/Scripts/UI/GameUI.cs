﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class GameUI : MonoBehaviour {

    public Text goldText;
    public Text warningText;
    public Image healthBar;
    public GameObject UpgradePanel;
    public float goldAcquired;
    public float hp;
    public float maxHp;


    void Start()
    {
        ShowHideUpgradePanel(false);
    }

	void Update () {

        healthBar.GetComponent<Image>().fillAmount = hp / maxHp;
        if (hp <= 0)
        {
            SceneManager.LoadScene(1);
        }
            goldText.text = "Gold: " + Mathf.Round(goldAcquired);
	}

    public void ShowHideUpgradePanel(bool enable)
    {
        UpgradePanel.SetActive(enable);
    }
}
