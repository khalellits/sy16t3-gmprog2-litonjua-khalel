﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems; 


public class Slot : MonoBehaviour, IPointerDownHandler
    {


    PlaceTower placeTower;
    public GameObject tower;
    public TowerData towerData;
    public GameObject[] openSpot;
    public int slotID;

    public SelectedSlot  SelectedSlot ;

	// Use this for initialization
	void Start () {

        SelectedSlot =  GameObject.FindObjectOfType<SelectedSlot>();
        towerData = tower.GetComponent<TowerData>();

    }

    public void OnPointerDown(PointerEventData data)
    {
            SelectedSlot.TowerChoice(tower, towerData);
    }

}
