﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UpgradeTower : MonoBehaviour, IPointerDownHandler {

	public GameUI GameUI;
	public PlaceTower PlaceTower; 
	public BuildingDuration BuildingDuration;

	// Use this for initialization
	void Start () {
		
		GameUI = FindObjectOfType<GameUI>();

	}
	
	public void OnPointerDown(PointerEventData data)
    {
		if (PlaceTower != null)
		{
			if (GameUI.goldAcquired > PlaceTower.monster.GetComponent<TowerData>().CurrentLevel.cost)
			{
				PlaceTower.BuildingDuration.StartCoroutine(PlaceTower.BuildingDuration.BuildingCoroutine());
				GameUI.goldAcquired -= PlaceTower.monster.GetComponent<	TowerData>().CurrentLevel.cost;
			}
	}

			PlaceTower = null;
			
    }
}
