﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlaceTower : MonoBehaviour {

    public GameObject monsterPrefab;
    public GameObject monster;
    public TowerData towerData;
    public GameObject gameManager;
    public GameUI gameUI;
    public int towerPrice;

    public BuildingDuration BuildingDuration;


    private int prefabNumber;

    public bool canPlaceMonster()
    {
        return monster == null;
    }

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        gameUI = gameManager.GetComponent<GameUI>();
        BuildingDuration = gameObject.GetComponent<BuildingDuration>();
    }
    
    public bool canUpgradeMonster()
    {
        if (monster != null)
        {
            TowerData monsterData = monster.GetComponent<TowerData>();
            TowerLevel nextLevel = monsterData.getNextLevel();
            if (nextLevel != null)
            {
                return true;
            }
        }
        return false;
    }

    public void PlaceMonster()
    {
            if (monsterPrefab != null)
            {
                if (gameUI.goldAcquired >= towerPrice)
                {
                    gameUI.warningText.enabled = false;
                    monster = (GameObject)
                    Instantiate(monsterPrefab, transform.position, Quaternion.identity);
                    gameUI.goldAcquired -= towerPrice;
                }
           
            }
    }

     public void UpgradeMonster()
     {
        gameUI.ShowHideUpgradePanel(true);
        gameUI.UpgradePanel.GetComponent<UpgradeTower>().PlaceTower = this.GetComponent<PlaceTower>();
        monster.GetComponent<TowerData>().increaseLevel();
     }
    

    public void Tower(GameObject tower, int towerCost)
    {
        monsterPrefab = tower;
        towerPrice = towerCost;
    }


}
