﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems; 


public class SuperPower :  MonoBehaviour
 {

    public GameObject[] enemies;
    public int Cost;
    public GameUI GameUI;

    public float MinDuration;
    public float MaxDuration;

    public Image CoolDown;

	// use this for initialization
	void Start () {

        GameUI = GameObject.FindObjectOfType<GameUI>();
	}

    void Update()
    {
        if (transform.position.y <= 0)
        {
            Destroy(this.gameObject);
        }

        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        CoolDown.fillAmount = MinDuration / MaxDuration;
        MinDuration += Time.deltaTime * 10f;

        if (MinDuration >= 100)
        {
            MinDuration = 100;
        }
    }
	
     public void ActivateApocalypse()
    {
        if (MinDuration >= 100)
        {
            Debug.Log("CLICKED");
            Apocalypse();
            MinDuration = 0;
        }
    }

    public void Apocalypse()
    {
        if (GameUI.goldAcquired >= Cost)
        {
            foreach (GameObject enemy in enemies)
            {
                enemy.GetComponent<EnemyHealth>().currHp = 0;
                enemy.GetComponent<Enemy>().Gold();
            }
            GameUI.goldAcquired -= Cost;
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            if (col.GetComponent<EnemyType>().Boss == Boss.Yes)
            {
                col.GetComponent<EnemyHealth>().currHp = col.GetComponent<EnemyHealth>().currHp - col.GetComponent<EnemyHealth>().currHp * .10f;
            }

            if (col.GetComponent<EnemyType>().Boss == Boss.No)
            {
                //currhp - currhp / .85f
                col.GetComponent<EnemyHealth>().currHp = col.GetComponent<EnemyHealth>().currHp - col.GetComponent<EnemyHealth>().currHp * .85f;
            }
        }
    }
}
