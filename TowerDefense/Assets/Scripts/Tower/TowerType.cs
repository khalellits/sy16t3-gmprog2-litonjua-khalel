﻿using UnityEngine;
using System.Collections;

public enum TowerTypes
{
    Crossbow,
    Cannon,
    Fire,
    Ice
}

public class TowerType : MonoBehaviour
{
    public TowerTypes Type;

}