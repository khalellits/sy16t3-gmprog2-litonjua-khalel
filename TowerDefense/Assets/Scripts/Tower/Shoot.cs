﻿ using UnityEngine;
 using System.Collections;

public class Shoot : MonoBehaviour
{
    public Transform BulletSpawnPoint;
    public TowerData towerData;
    public BulletData bulletData;
    public Transform Enemy;
    public float range = 50.0f;
    public float bulletImpulse = 20.0f;
    public Rigidbody projectile;

    private bool onRange = false;
    private float initialRate;
    public float fireRate;


    void Start()
    {
        towerData = gameObject.GetComponent<TowerData>();
        initialRate = fireRate;
 
    }

    void ShootEnemy()
    {
        //fire rate
        fireRate = towerData.currentLevel.fireRate;
        //projectile
        projectile = towerData.currentLevel.bullet;
        //bullet damage 
        projectile.GetComponent<BulletData>().bulletDamage = towerData.currentLevel.damage;  
        //chill amount
        projectile.GetComponent<BulletData>().chillAmount = towerData.currentLevel.chillAmount;
        //damage over time
        projectile.GetComponent<BulletData>().damageOverTime = towerData.currentLevel.dmgOverTime;
        //effectDuration
        projectile.GetComponent<BulletData>().effectDuration = towerData.currentLevel.effecDuration;
        //area effect
        projectile.GetComponent<BulletEffect>().areaEffect = towerData.currentLevel.areaEffect;
        Rigidbody bullet = Instantiate(projectile, BulletSpawnPoint.position, transform.rotation) as Rigidbody;
        bullet.velocity = (Enemy.position - transform.position).normalized * bulletImpulse;
        Destroy(bullet.gameObject, 0.5f);

    }

    void Update()
    {

        if (Enemy != null && initialRate > 0)
        {
            initialRate -= Time.deltaTime;
        }
        if (initialRate <= 0 && Enemy != null)
        {
            onRange = Vector3.Distance(transform.position, Enemy.transform.position) < range;
            if (onRange)
            {
                ShootEnemy();
                initialRate = fireRate;
            }

            else
            {
                return;
            }
        }
    }
  
}

