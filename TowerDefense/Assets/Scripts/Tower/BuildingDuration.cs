﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingDuration : MonoBehaviour {

	public Image BuildingDurationUI;
	public GameObject GameManager;
	public GameUI GameUI;
	public PlaceTower PlaceTower;
	public SelectedSlot SelectedSlot;
	public int Duration = 5;

	public bool active;

    public float FillAmount;

	void Start () {

		GameManager = GameObject.Find("GameManager");
		GameUI = GameManager.GetComponent<GameUI>();
		BuildingDurationUI = gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>();
		PlaceTower = gameObject.GetComponent<PlaceTower>();
		SelectedSlot = GameManager.GetComponent<SelectedSlot>();
	}


	public IEnumerator BuildingCoroutine()
	{

		float startTime = Time.time;
        float startFillAmount = 0;
        while (Time.time < startTime + Duration)
        {
			active = true;
            FillAmount = Mathf.Lerp(startFillAmount, 1.0F, (Time.time - startTime) / Duration);
            BuildingDurationUI.fillAmount = FillAmount;
            yield return null;
        }

		   active = false;
		  
		   if (PlaceTower.canPlaceMonster())
		   {
				PlaceTower.PlaceMonster();
		   }

		   else if (PlaceTower.canUpgradeMonster())
		   {
			   PlaceTower.UpgradeMonster();
		   }
		   
		   BuildingDurationUI.fillAmount = 0;
		
		
		   GameUI.ShowHideUpgradePanel(false);
	}



	void OnMouseUp()
	{
	  	
		PlaceTower.monsterPrefab = SelectedSlot.TowerSelected;

		if (SelectedSlot.TowerData != null)
		{
			PlaceTower.towerData = SelectedSlot.TowerData;
			PlaceTower.towerPrice = SelectedSlot.TowerData.CurrentLevel.cost;
		}

		  if (PlaceTower.canPlaceMonster() && PlaceTower.monsterPrefab != null)
		  {
			  if (active == false)
			  {
				StartCoroutine(BuildingCoroutine());
			  }
		  }

		  else if (PlaceTower.canUpgradeMonster())
		  {
			GameUI.ShowHideUpgradePanel(true);
            GameUI.UpgradePanel.GetComponent<UpgradeTower>().PlaceTower = this.GetComponent<PlaceTower>();
		  }
	}
	
}
