﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour {

    public GameObject target;
    public GameObject[] enemies;
    public Shoot shoot;
    public Enemytypes TargetType;
    public float range;
    private bool onRange = false;

    // Use this for initialization
    void Start()
    {       
        
    }
    // Update is called once per frame
    void Update()
    {
        shoot = gameObject.GetComponent<Shoot>();
        range = shoot.range;
        EnemyDist();
    }

    void EnemyDist()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        int closestIndex = 0;
        float nearestDistance = Mathf.Infinity;
        float closestEnemy;
        for (int i = 0; i < enemies.Length; i++)
        {
            onRange = Vector3.Distance(transform.position, enemies[i].transform.position) < range;
            closestEnemy = Vector3.Distance(transform.position, base.transform.position);

            if (closestEnemy < nearestDistance && onRange)
            {
                // Debug.Log ("Enemy Spawned",target);
                nearestDistance = closestEnemy;
                closestIndex = i;
                target = enemies[i];
                target.name = "Enemy" + i;

                //Tower Type
                 if (gameObject.GetComponent<TowerType>().Type == TowerTypes.Crossbow)
                {
                    if (target.GetComponent<EnemyType>().Type != TargetType)
                    {
                        target = null;
                        return;
                    }
                }


                if (gameObject.GetComponent<TowerType>().Type == TowerTypes.Cannon)
                {
                    if (target.GetComponent<EnemyType>().Type != TargetType)
                    {
                        target = null;
                        return;
                    }
                }

                if (gameObject.GetComponent<TowerType>().Type == TowerTypes.Ice)
                {

                    if (target.GetComponent<Enemy>().chilled == true)
                    {
                        target = null;
                        return;
                    }

                }


                if (gameObject.GetComponent<TowerType>().Type == TowerTypes.Fire)
                {

                    if (target.GetComponent<Enemy>().burned == true)
                    {
                        target = null;
                        return;
                    }

                }

                     transform.LookAt(target.transform);             
            }

        }

        if (target != null)
        {

            GetComponent<Shoot>().Enemy = target.transform;
        }
        else
        {
            GetComponent<Shoot>().Enemy = null;
        }
            
    }


}
