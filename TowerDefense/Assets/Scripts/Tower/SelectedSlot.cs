﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedSlot : MonoBehaviour {

	public GameObject TowerSelected;
	public TowerData TowerData;

	// Use this for initialization


	public void TowerChoice(GameObject tower, TowerData data)
	{
		TowerSelected = tower;
		TowerData = data;
	}
	
	

}
