﻿using UnityEngine;
using System.Collections;

public enum BulletTypes
{
    Crossbow,
    Cannon,
    Fire,
    Ice
}

public class BulletType : MonoBehaviour
{
    public BulletTypes Type;
}
