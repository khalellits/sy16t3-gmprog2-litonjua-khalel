﻿using UnityEngine;
using System.Collections;

public class BulletData : MonoBehaviour {

   
    public float bulletDamage;
    public float chillAmount;
    public float effectDuration;
    public float damageOverTime;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log("Damage: " + bulletDamage + " Slow Level: " + chillAmount + " DOT: " + damageOverTime);
	}
}
