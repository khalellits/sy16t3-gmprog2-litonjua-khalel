﻿using UnityEngine;
using System.Collections;


public class BulletEffect : MonoBehaviour {


    BulletData bulletData;
    TowerData towerData;
    EnemyHealth enemyMoveSpeed;
    public float areaEffect;

    void Start()
    {
        bulletData = gameObject.GetComponent<BulletData>();
        towerData = GetComponent<TowerData>();
    }

    public void ExplosionDamage(Collider other, Vector3 center, float radius)
    {   
        
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);

        foreach (Collider col in hitColliders)
        {
            if (col.gameObject.tag.Equals("Enemy"))
            {
                col.GetComponent<Enemy>().maxEffectDuration = bulletData.GetComponent<BulletData>().effectDuration;
                col.GetComponent<Enemy>().dmgOverTime = bulletData.GetComponent<BulletData>().damageOverTime;
                col.GetComponent<Enemy>().chillAmount = bulletData.GetComponent<BulletData>().chillAmount;

                if (gameObject.GetComponent<BulletType>().Type == BulletTypes.Cannon)
                {
                    col.GetComponent<EnemyHealth>().currHp -= bulletData.bulletDamage;
                }

                if (gameObject.GetComponent<BulletType>().Type == BulletTypes.Fire)
                {
                    col.GetComponent<Enemy>().burned = true;
                    col.GetComponent<EnemyHealth>().currHp -= bulletData.bulletDamage;
                    //Debug.Log("AOE: " + areaEffect + "DMG: " + bulletData.bulletDamage + "dmg over time" + bulletData.damageOverTime + "Burned: " + col.GetComponent<Enemy>().burned);
                }

                if (gameObject.GetComponent<BulletType>().Type == BulletTypes.Ice)
                {
                    col.GetComponent<Enemy>().chilled = true;
                }
               
            }
        }    
    }

}
