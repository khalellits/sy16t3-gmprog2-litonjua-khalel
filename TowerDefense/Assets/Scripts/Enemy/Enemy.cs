﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    TowerData towerData;
    BulletData bulletData;
    BulletEffect bulletEffect;
    EnemyHealth health;
    EnemyMoveSpeed moveSpeed;
    public GameObject gameManager;

    public GameObject particle;

    public bool chilled;
    public bool burned;

    public float chillAmount;
    public float dmgOverTime;
    public float currDuration;
    public float maxEffectDuration;

    public float goldBonus;

	void Start () {

        gameManager = GameObject.Find("GameManager");

        if (particle != null)
        {
            particle.SetActive(false);
        }
            health = gameObject.GetComponent<EnemyHealth>();
            moveSpeed = gameObject.GetComponent<EnemyMoveSpeed>();
            goldBonus = gameObject.GetComponent<EnemyHealth>().currHp / 10f;    
       
	}
	
    public void Gold()
    {
        if (health.currHp <= 0)
        {
            gameManager.GetComponent<GameUI>().goldAcquired += goldBonus;
            Destroy(gameObject);
        
        }
    }

	void Update () {

        Gold();

        if (chilled == true)
        {
            moveSpeed.slow = chillAmount;
            currDuration += Time.deltaTime;

            if (currDuration >= maxEffectDuration)
            {
                currDuration = 0;
                moveSpeed.slow = 0;
                chilled = false;
            }
        }

        if (burned == true)
        {

            particle.SetActive(true);
            currDuration += Time.deltaTime;
            health.currHp -= dmgOverTime;

            if (currDuration >= maxEffectDuration)
            {
                currDuration = 0;
                burned = false;
                particle.SetActive(false);
            }
        }


	}


    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Destination")
        {
            gameManager.GetComponent<GameUI>().hp -= 10;
            Destroy(gameObject);
            Debug.Log("ENTERED THE DRAGON");
        }

     
        if (other.gameObject.tag.Equals("Bullet"))
        {
            //area of effect and damage

            gameObject.GetComponent<EnemyHealth>().currHp -= other.GetComponent<BulletData>().bulletDamage; 

            other.GetComponent<BulletEffect>().ExplosionDamage(gameObject.GetComponent<Collider>(), gameObject.transform.position, other.GetComponent<BulletEffect>().areaEffect); other.GetComponent<BulletEffect>().ExplosionDamage(gameObject.GetComponent<Collider>(), gameObject.transform.position, other.GetComponent<BulletEffect>().areaEffect);

            if (other.GetComponent<BulletType>().Type == BulletTypes.Fire)
            {
                dmgOverTime = other.GetComponent<BulletData>().damageOverTime;
                maxEffectDuration = other.GetComponent<BulletData>().effectDuration;
                burned = true;
            }

            if (other.GetComponent<BulletType>().Type == BulletTypes.Ice)
            {
                chillAmount = other.GetComponent<BulletData>().chillAmount;
                chilled = true;
            }

            Destroy(other.gameObject);   
        }
    }
}
