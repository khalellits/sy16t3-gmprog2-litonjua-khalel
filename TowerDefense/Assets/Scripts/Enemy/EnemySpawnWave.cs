﻿using UnityEngine;
using System.Collections;

public class EnemySpawnWave : MonoBehaviour 
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform Enemy;
        public int count;
        public float rate;
    }
    public Wave[] waves;
    private int nextWave = 0;
    public Transform[] spawnPoints;
    public float timeBetweenWaves = 5f;
    public float waveCountdown;
    private float searchCountDown = 1f;
    //private float SearchBoss = 1f;
    private SpawnState state = SpawnState.COUNTING;

    private GameManager gameManager;

    void Start()
    {
        waveCountdown = timeBetweenWaves;
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>(); 
    }

    void Update()
    {
        if(state == SpawnState.WAITING)
        {
            //check if enemies are still alive

            if (!EnemyIsAlive())
            {
                BeginNextWave();
            }
            else
            {
                return;
            }

        }

        if(waveCountdown <= 0)
        {
            if(state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    void BeginNextWave()
    {
        Debug.Log("Wave Completed");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if(nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("All Waves Complete");
        }
        else
        {
            gameManager.gameOver = true;
            nextWave++;
            gameManager.Wave++; // Text WAVE UI

        }

    }

    bool EnemyIsAlive()
    {
        searchCountDown -= Time.deltaTime;

        if(searchCountDown <= 0)
        {
            searchCountDown = 1;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }
    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave" + _wave.name);

        state = SpawnState.SPAWNING;

        //Spawn
        for (int i = 0; i < _wave.count; i++ )
        {
            SpawnEnemy(_wave.Enemy);
            yield return new WaitForSeconds(1.0f / _wave.rate);
        }

            state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        // Spawn Enemy
        Debug.Log("Spawning Enemy: " + _enemy.name);

        Instantiate(_enemy, transform.position, Quaternion.Euler(0,-90f,0));
        Transform _sp = spawnPoints[ Random.Range (0,spawnPoints.Length) ];
    }
}
