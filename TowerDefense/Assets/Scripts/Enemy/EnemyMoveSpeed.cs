﻿using UnityEngine;
using System.Collections;

public class EnemyMoveSpeed : MonoBehaviour {

    public float defaultMoveSpeed;
    public float moveSpeed;
    public float slow;
    public UnityEngine.AI.NavMeshAgent navMesh;

    void Start()
    {
        navMesh = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
       
        defaultMoveSpeed = moveSpeed - slow;

        if (defaultMoveSpeed <= 0)
        {
            defaultMoveSpeed = 0.5f;
        }

        navMesh.speed = defaultMoveSpeed;
       
      
    }
    
}
