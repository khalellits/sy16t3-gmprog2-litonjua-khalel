﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    public GameObject EnemyPrefab;
    private float enemyCount = 0;
    public float MinMass = 0.3f;
    public float MaxMass = 5.0f;
    public Vector3 MinSpawnPoint;
    public Vector3 MaxSpawnPoint;
    public float MinSpawnInterval = 1.0f;
    public float MaxSpawnInterval = 3.0f;

    void Start()
    {
        StartCoroutine(SpawnTask());
    }

    void update()
    {
        this.transform.position = new Vector3
            (Mathf.Clamp(this.transform.position.x, -15f, 15f),
                Mathf.Clamp(this.transform.position.y, -15f, 15f),
                0);
    }
    IEnumerator SpawnTask()
    {
        while (enemyCount <= 5)
        {
            float waitTime = Random.Range(MinSpawnInterval, MaxSpawnInterval);
            Debug.Log("Spawn Enemy: " + enemyCount);
            enemyCount++;
            yield return new WaitForSeconds(waitTime);

            SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        GameObject Enemy = Instantiate(EnemyPrefab) as GameObject;

        float x = Random.Range(MinSpawnPoint.x, MaxSpawnPoint.x);
        float y = Random.Range(MinSpawnPoint.y, MaxSpawnPoint.y);
        float z = Random.Range(MinSpawnPoint.z, MaxSpawnPoint.z);
        Enemy.transform.position = new Vector3(x, y, z);
    }
}

