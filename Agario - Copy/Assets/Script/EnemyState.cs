﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Eat,
    Run,
    Patrol,
}

public class EnemyState : MonoBehaviour
{
    public State curState;
    //public Transform nearByEnemy;
    public float Speed;
    private Vector3 randomTarget;

    GameObject Player;
    GameObject Food;

    float FoodDist;
    float PlayerDist;

    void Update()
    {
        switch (curState)
        {

            case State.Patrol: UpdatePatrol(); break;
            case State.Eat: UpdateEat(); break;
            case State.Run: UpdateRun(); break;
        }

    }

    void UpdateEat()
    {

        //move toward nearByEnemy
        FindDistance();

        Player = nearByEnemy("Player");
        Food = nearByEnemy("Food");

        if (PlayerDist > FoodDist && Food.transform.localScale.x < gameObject.transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, Food.transform.position, Speed * Time.deltaTime / transform.localScale.x);
            this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
            Debug.Log("Eat");
        }

        //if the target Destroyed
        //change the state to Patrol :(((((((((((((((((((((((
        if (Food == null)
        {
            UpdatePatrol();
        }

        //if nearByEnemy change his scale bigger than you
        // change the state to run
        if (PlayerDist < FoodDist && Player.transform.localScale.x > gameObject.transform.localScale.x)
        {
            UpdateRun();
        }
    }

    void UpdateRun()
    {
        // check the distance if the enemy is faraway 
        // state patrol

        // if distance is close i will continue running
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, -Speed * Time.deltaTime / transform.localScale.x);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
        Debug.Log("Run");
    }

    void UpdatePatrol()
    {

        // get random point where we need to gp
        // if i reach the point generate point
        if (Vector3.Distance(this.transform.position, randomTarget) < 2)
        {
            float posX = Random.Range(-10f, 10f);
            float posY = Random.Range(-10f, 10f);
            randomTarget = new Vector3(posX, posY, 0);
        }
        this.transform.position = Vector3.MoveTowards(this.transform.position, randomTarget, Time.deltaTime * Speed);

        //if the nearbyEnemy is smaller than you
        //change the state to Eat
        if (PlayerDist > FoodDist && Player.transform.localScale.x < gameObject.transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime / transform.localScale.x);
            this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
            Debug.Log("Eat");
        }

        //if the nearbyEnemy is larger than you
        // change the state to Run
        if (PlayerDist < FoodDist && Player.transform.localScale.x > gameObject.transform.localScale.x)
        {
            UpdateRun();
        }

    }
    // detection you can explore SphereCast //invisible collider
    void FindDistance()
    {
        FoodDist = Vector3.Distance(transform.position, nearByEnemy("Food").transform.position);
        PlayerDist = Vector3.Distance(transform.position, nearByEnemy("Player").transform.position);
    }

    GameObject nearByEnemy(string Tag)
    {
        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag(Tag);
        GameObject nearest = null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gameObjects)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                nearest = go;
                distance = curDistance;
            }
        }
        return nearest;
    }
}
