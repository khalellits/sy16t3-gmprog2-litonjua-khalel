﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour
{
    public GameObject player;
    public float Speed = 0;
    private float CamScale;


    void Start()
    {

    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Speed * Time.deltaTime);
        
        Vector3 cameralock = transform.position;
        cameralock.z = Mathf.Clamp(0, 0, -10);
        transform.position = cameralock;

        if (player.transform.localScale.x > CamScale)
        {
            Camera.main.orthographicSize += .10f;
            CamScale = player.transform.localScale.x;
        }
    }
}