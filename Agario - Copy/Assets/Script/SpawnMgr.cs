﻿using UnityEngine;
using System.Collections;

public class SpawnMgr : MonoBehaviour
{
    public GameObject FoodPrefab;
    private float foodCnt = 0;
    public float MinMass = 0.3f;
    public float MaxMass = 5.0f;
    public Vector3 MinSpawnPoint;
    public Vector3 MaxSpawnPoint;
    public float MinSpawnInterval = 1.0f;
    public float MaxSpawnInterval = 3.0f;

    void Start()
    {
        for (int x = 0; x < 30; x++)
        {
            SpawnFood();
        }
        StartCoroutine(SpawnTask());
    }

    void update()
    {
        this.transform.position = new Vector3
            (Mathf.Clamp(this.transform.position.x, -17f, 17f),
                Mathf.Clamp(this.transform.position.y, -17f, 17f),
                0);
    }
    IEnumerator SpawnTask()
    {
        while (foodCnt <= 10)
        {
            float waitTime = Random.Range(MinSpawnInterval, MaxSpawnInterval);
            Debug.Log("Spawn Food: " + foodCnt);
            foodCnt++;
            yield return new WaitForSeconds(waitTime);

            SpawnFood();
        }
    }

    void SpawnFood()
    {
        GameObject Food = Instantiate(FoodPrefab) as GameObject;

        float x = Random.Range(MinSpawnPoint.x, MaxSpawnPoint.x);
        float y = Random.Range(MinSpawnPoint.y, MaxSpawnPoint.y);
        float z = Random.Range(MinSpawnPoint.z, MaxSpawnPoint.z);
        Food.transform.position = new Vector3(x, y, z);
    }
}

