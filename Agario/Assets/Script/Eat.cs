﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Eat : MonoBehaviour
{
    public Text Letters;
    public float Increase;

    int Score = 0;

    void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject) && this.transform.localScale.x > other.transform.localScale.x)
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);

            Score += 1;
            Letters.text = "SCORE: " + Score;
        }
    }
}