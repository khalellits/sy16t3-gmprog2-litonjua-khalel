﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Eat,
    Run,
    Patrol,
}

public class EnemyState : MonoBehaviour
{
    public State curState;
    //public Transform nearByEnemy;
    public float Speed;
<<<<<<< HEAD
=======
    private Vector3 randomTarget;
>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c

    GameObject Player;
    GameObject Food;

<<<<<<< HEAD
    float FoodDistance;
    float PlayerDistance;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
=======
    float FoodDist;
    float PlayerDist;
>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c

    void Update()
    {
        switch (curState)
        {

            case State.Patrol: UpdatePatrol(); break;
            case State.Eat: UpdateEat(); break;
            case State.Run: UpdateRun(); break;
        }
<<<<<<< HEAD
        
    }

    void FindDistance()
    {
        FoodDistance = Vector3.Distance(transform.position, nearByEnemy("Food").transform.position);
        PlayerDistance = Vector3.Distance(transform.position, nearByEnemy("Player").transform.position);
    }

    GameObject nearByEnemy(string Tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(Tag);
        GameObject closest = null;

        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
=======

>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c
    }

    void UpdateEat()
    {
<<<<<<< HEAD
        //move toward nearByEnemy 
        Food = nearByEnemy("Food");
        FindDistance();

        if (FoodDistance < PlayerDistance && Player.transform.localScale.x < gameObject.transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime / transform.localScale.x);
            this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
            Debug.Log("CHASING");
        }
        
        //if the target Destroyed
        //change the state to Patrol
        if (Food = null)
=======

        //move toward nearByEnemy
        FindDistance();

        Player = nearByEnemy("Player");
        Food = nearByEnemy("Food");

        if (PlayerDist > FoodDist && Player.transform.localScale.x < gameObject.transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime / transform.localScale.x);
            this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
            Debug.Log("Eat");
        }

        //if the target Destroyed
        //change the state to Patrol :(((((((((((((((((((((((
        if (Food == null)
>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c
        {
            UpdatePatrol();
        }

        //if nearByEnemy change his scale bigger than you
        // change the state to run
<<<<<<< HEAD
        if (PlayerDistance < FoodDistance && Player.transform.localScale.x > gameObject.transform.localScale.x)
=======
        if (PlayerDist < FoodDist && Player.transform.localScale.x > gameObject.transform.localScale.x)
>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c
        {
            UpdateRun();
        }
    }

    void UpdateRun()
    {
        // check the distance if the enemy is faraway 
        // state patrol

        // if distance is close i will continue running
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, -Speed * Time.deltaTime / transform.localScale.x);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
<<<<<<< HEAD
        Debug.Log("RUNNING");
=======
        Debug.Log("Run");
>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c
    }

    void UpdatePatrol()
    {
<<<<<<< HEAD
        transform.position = Vector3.MoveTowards(transform.position, Food.transform.position, Speed * Time.deltaTime / transform.localScale.x);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
        Debug.Log("PATROL");
=======

>>>>>>> 0128d2f940c26ce10359731fdab6ef32b679bf8c
        // get random point where we need to gp
        // if i reach the point generate point
        if (Vector3.Distance(this.transform.position, randomTarget) < 2)
        {
            float posX = Random.Range(-10f, 10f);
            float posY = Random.Range(-10f, 10f);
            randomTarget = new Vector3(posX, posY, 0);
        }
        this.transform.position = Vector3.MoveTowards(this.transform.position, randomTarget, Time.deltaTime * Speed);

        //if the nearbyEnemy is smaller than you
        //change the state to Eat
        if (PlayerDist > FoodDist && Player.transform.localScale.x < gameObject.transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime / transform.localScale.x);
            this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
            Debug.Log("Eat");
        }

        //if the nearbyEnemy is larger than you
        // change the state to Run
        if (PlayerDist < FoodDist && Player.transform.localScale.x > gameObject.transform.localScale.x)
        {
            UpdateRun();
        }

    }
    // detection you can explore SphereCast //invisible collider
    void FindDistance()
    {
        FoodDist = Vector3.Distance(transform.position, nearByEnemy("Food").transform.position);
        PlayerDist = Vector3.Distance(transform.position, nearByEnemy("Player").transform.position);
    }

    GameObject nearByEnemy(string Tag)
    {
        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag(Tag);
        GameObject nearest = null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gameObjects)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                nearest = go;
                distance = curDistance;
            }
        }
        return nearest;
    }
}
