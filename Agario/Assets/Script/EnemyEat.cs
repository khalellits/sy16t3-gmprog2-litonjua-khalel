﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyEat : MonoBehaviour
{
    public float Increase;

    void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.tag == "Player") && this.transform.localScale.x > other.transform.localScale.x)
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);
        }

        if ((other.gameObject.tag == "Enemy") && this.transform.localScale.x > other.transform.localScale.x)
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);
        }

        if ((other.gameObject.tag == "Food") && this.transform.localScale.x > other.transform.localScale.x)
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);
        }
    }
}