﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public GameObject PrefabBullet;
    public float movementSpeed = 10.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject objBullet;
            objBullet = (GameObject)Instantiate(PrefabBullet, this.transform.position, Quaternion.identity);
            Destroy(objBullet, 2);
        }
    }
}