﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour
{
    public Transform target;
    public float Speed;

    void Update()
    {
        float speed = Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed);

        if (transform.position.y == target.position.y)
        {
            transform.position = new Vector3(1, 100, 0);
        }
    }
}