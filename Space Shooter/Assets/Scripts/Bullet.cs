﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    float bulletSpeed = 10;
    public int Asteroid = 1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.up * bulletSpeed * Time.deltaTime);
    }

    //void OnCollisionEnter(Collision other)
    //{
    //    Debug.Log("Hit! " + other.gameObject.name);
    //}

    //without physical contact
    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        Destroy(this.gameObject);
        Debug.Log("Hit! " + other.name);
        ScoreManager.score += Asteroid;
    }
}
