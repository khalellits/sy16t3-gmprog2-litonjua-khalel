﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour
{
    public GameObject AsteroidPrefab;
    public float MinMass = 0.3f;
    public float MaxMass = 5.0f;
    public Vector3 MinSpawnPoint;
    public Vector3 MaxSpawnPoint;
    public float MinSpawnInterval = 1.0f;
    public float MaxSpawnInterval = 3.0f;

    void Start()
    {
        StartCoroutine(SpawnTask());
    }

    IEnumerator SpawnTask()
    {
        while (true)
        {
            float waitTime = Random.Range(MinSpawnInterval, MaxSpawnInterval);
            Debug.Log(waitTime);
            yield return new WaitForSeconds(waitTime);
            SpawnAsteroid();
        }
    }

    void SpawnAsteroid()
    {
        GameObject asteroid = Instantiate(AsteroidPrefab);

        float x = Random.Range(MinSpawnPoint.x, MaxSpawnPoint.x);
        float y = Random.Range(MinSpawnPoint.y, MaxSpawnPoint.y);
        float z = Random.Range(MinSpawnPoint.z, MaxSpawnPoint.z);
        asteroid.transform.position = new Vector3(x, y, z);

        asteroid.GetComponent<Rigidbody>().mass = Random.Range(MinMass, MaxMass);
        asteroid.name = "Asteroid";
    }
}