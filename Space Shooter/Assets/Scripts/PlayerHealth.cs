﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 5;                         
    public int currentHealth;                           
    public Slider healthSlider;                          
    public Image damageImage;
    public Image healImage;
    public float flashSpeed = 5f;                               
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    public Color flashHealColour = new Color(0f, 0f, 1f, 0.1f);
    PlayerHealth playerHealth;
    PlayerMovement playerMovement;                               
    bool isDead;                                                
    bool damaged;
    bool healed;


    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        currentHealth = startingHealth;
        playerHealth = GetComponent<PlayerHealth>();
    }


    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;

        if (healed)
        {
            healImage.color = flashColour;
        }
        else
        {
            healImage.color = Color.Lerp(healImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        healed = false;
    }


    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;
        healthSlider.value = currentHealth;
        
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    public void TakePower(int amount)
    {
        healed = true;
        currentHealth += amount;
        healthSlider.value = currentHealth;
    }

    void Death()
    {
        isDead = true;
        playerMovement.enabled = false;
        Destroy(this.gameObject);
        Application.LoadLevel("GameOver");
    }
}